Web Rekrutmen
Web ini dibuat untuk memudahkan Personalia menginput dan memonitoring calon tanaga kerja(Harian) baru dan karyawan baru,
dan diprogram ini personalian bisa memonitoring hasil wawancara atau karyawan yang bermasalah.
Web Rekrutmen juga dibuat agar setiap departemen mengetahui calon tenaga kerja atau karyawan yang akan diwawancarai.
Setiap departemen bisa menginputkan permintaan calon tenaga kerja atau karyawan jika departemen tersebut kekurangan personil.

pembuatan Web Rekrutmen menggunakan:
- Framework CodeIgniter 3
- Database Sql Server 2008
- Bootstrap
- HTML
- CSS
- JS
- Ajax
- JSON
- jQuery
